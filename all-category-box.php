<section class="shortlist-category">
      <div class="container">
        <div class="all-in-one-title">
            <h4>Explore more Top10.com Categories</h4>
            <a href="all-list.php" class="all-shortlist-link">View all shortlists</a>
        </div>
        <div class="shortlist-category-list">
          <ul>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Finance
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Tech
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">lifestyle
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Household
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Health & Wellness
                  </span>
              </a>
            </li>

          </ul>

        </div>
         <div class="shortlist-category-list">
          <ul>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Family
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Business Tools
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Shopping
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Entertainment
                  </span>
              </a>
            </li>
            <li>
              <a class="categories-shortlist--category__link" href="category.php">
                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
                  <span class="categories-shortlist--category__text">Personal Growth
                  </span>
              </a>
            </li>

          </ul>
          
        </div>
      </div>
    </section>