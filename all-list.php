<?php include 'header.php';?>

	<main>
	<section class="all-category-list">
		<div class="shortlist-category">
	    	<div class="container">
		        <div class="all-in-one-title">
		            <h2>Every Top10 Shortlist, all in one place</h2>
		            <!-- <a href="all-list.php" class="all-shortlist-link">View all shortlists</a> -->
		        </div>
		        <div class="shortlist-category-list">
		          <ul>
		            <li>
		              <button type="button" class="categories-shortlist--category__link">
		                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
		                  <span class="categories-shortlist--category__text">Finance
		                  </span>
		                  <input type="checkbox" onclick="f1()" id="vehicle1" name="" value="">
		              </button>
		            </li>

		            <li>
		              <button type="button" class="categories-shortlist--category__link">
		                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
		                  <span class="categories-shortlist--category__text">Tech
		                  </span>
		                  <input onclick="f2()" type="checkbox" id="vehicle1" name="" value="">
		              </button>
		            </li>

		            <li>
		              <button type="button" class="categories-shortlist--category__link">
		                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
		                  <span class="categories-shortlist--category__text">Lifestyle
		                  </span>
		                  <input onclick="f3()" type="checkbox" id="vehicle1" name="" value="">
		              </button>
		            </li>

		            <li>
		              <button type="button" class="categories-shortlist--category__link">
		                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
		                  <span class="categories-shortlist--category__text">Household
		                  </span>
		                  <input onclick="f4()" type="checkbox" id="vehicle1" name="" value="">
		              </button>
		            </li>

		            <li>
		              <button type="button" class="categories-shortlist--category__link">
		                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
		                  <span class="categories-shortlist--category__text">Health & Wellness
		                  </span>
		                  <input onclick="f5()" type="checkbox" id="vehicle1" name="" value="">
		              </button>
		            </li>
		            
		          </ul>
			   	</div>
		        <div class="shortlist-category-list">
		          	<ul>
		            	<li>
				            <button type="button" class="categories-shortlist--category__link">
				                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
				                  <span class="categories-shortlist--category__text">Family
				                  </span>
				                  <input onclick="f6()" type="checkbox" id="vehicle1" name="" value="">
		              		</button>
		            	</li>

		            	<li>
				            <button type="button" class="categories-shortlist--category__link">
				                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
				                  <span class="categories-shortlist--category__text">Business Tools
				                  </span>
				                  <input onclick="f7()" type="checkbox" id="vehicle1" name="" value="">
		              		</button>
		            	</li>

		            	<li>
				            <button type="button" class="categories-shortlist--category__link">
				                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
				                  <span class="categories-shortlist--category__text">Shopping
				                  </span>
				                  <input onclick="f8()" type="checkbox" id="vehicle1" name="" value="">
		              		</button>
		            	</li>

		            	<li>
				            <button type="button" class="categories-shortlist--category__link">
				                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
				                  <span class="categories-shortlist--category__text">Entertainment
				                  </span>
				                  <input onclick="f9()" type="checkbox" id="vehicle1" name="" value="">
		              		</button>
		            	</li>

		            	<li>
				            <button type="button" class="categories-shortlist--category__link">
				                <img class="categories-shortlist--category__icon" src="https://images.top10.com/q_auto/v1/production/categories-directory/categories/ninja/finance.svg" alt="Finance" title="">
				                  <span class="categories-shortlist--category__text">Personal Growth
				                  </span>
				                  <input onclick="f10()" type="checkbox" id="vehicle1" name="" value="">
		              		</button>
		            	</li>

		          	</ul>
		          
		        </div>
	      </div>
	    </div>
	</section>
		
	<section class="single-category-list container" id="finance-list" style="display: none;">
		<div>
			<h6>
				Finance
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>


	<section class="single-category-list container" id="tech-list" style="display: none;">
		<div>
			<h6>
				Tech
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="life-style-list" style="display: none;">
		<div>
			<h6>
				Lifestyle
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="household-list" style="display: none;">
		<div>
			<h6>
				Household
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="health-list" style="display: none;">
		<div>
			<h6>
				Health & Wellness
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="family-list" style="display: none;">
		<div>
			<h6>
				Family
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="bussiness-list" style="display: none;">
		<div>
			<h6>
				Business Tools
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="shopping-list" style="display: none;">
		<div>
			<h6>
				Shopping
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="entertainment-list" style="display: none;">
		<div>
			<h6>
				Entertaintment
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>

	<section class="single-category-list container" id="personal-growth-list" style="display: none;">
		<div>
			<h6>
				Personal Growth
			</h6>
			<ul>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-plus-square" aria-hidden="true"></i>
						Automated Investment
					</a>
				</li>
				
				
			</ul>
		</div>
	</section>
	</main>



	<script>
	function f1() {
		  var x = document.getElementById("finance-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}

	function f2() {
		  var x = document.getElementById("tech-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f3() {
		  var x = document.getElementById("life-style-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f4() {
		  var x = document.getElementById("household-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f5() {
		  var x = document.getElementById("health-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f6() {
		  var x = document.getElementById("family");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f7() {
		  var x = document.getElementById("bussiness-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f8() {
		  var x = document.getElementById("shopping-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f9() {
		  var x = document.getElementById("entertainment-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
	function f10() {
		  var x = document.getElementById("personal-growth-list");
		  if (x.style.display === "none") {
		    x.style.display = "block";
		  } else {
		    x.style.display = "none";
		  }
	}
</script>
<?php include 'footer.php';?>

