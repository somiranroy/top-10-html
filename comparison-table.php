<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="css/table.css"> <!-- Resource style -->
  <script src="js/modernizr.js"></script> <!-- Modernizr -->
    
  <title>Products Comparison Table | CodyHouse</title>
</head>
<body>

  <section class="cd-products-comparison-table">
   <!--  <header>
      

      <div class="actions">
        <a href="#0" class="reset">Reset</a>
        <a href="#0" class="filter">Filter</a>
      </div>
    </header> -->

    <div class="cd-products-table">
      <div class="features">
        <div class="top-info">Compare Best Meal Kit Delivery Services</div>
        <ul class="cd-features-list">
          <li>Reviews</li>
          <li>Price <i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li>Shipping <i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li>Delivery areas <i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li>Great for <i class="fa fa-question-circle" aria-hidden="true"></i></li>
          <li>Special offers <i class="fa fa-question-circle" aria-hidden="true"></i></li>
        </ul>
      </div> <!-- .features -->
      
      <div class="cd-products-wrapper">
        <ul class="cd-products-columns">
          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li> <!-- .product -->

          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li>

          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li>

          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li>

          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li>

          <li class="product">
            <div class="top-info">
              <div class="compare-article-title">
                <div>
                  <span>1.</span>
                  <span>home Chef</span>
                </div>
                <div class="">
                  <img src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20181008114924.png" alt="Home Chef" title="">
                </div>
              </div>
              <div>
                <p>
                  Best for healthy eaters
                </p> 
                <a href="#" class="view-plan">View Plans</a>               
              </div>
            </div> <!-- .top-info -->

            <ul class="cd-features-list">
              <li class="rate"><span>3/5</span></li>
              <li>$600</li>
              <li class="flex column">Free
              <span class="color-gray">on orders over $45</span></li>
              <li class="flex column">US
              <span class="color-gray">except AK and HI</span></li>
              <li>Healthy eaters</li>
              <li>Get $30 off your purchase</li>
              
            </ul>
          </li>

           <!-- .product -->
        </ul> <!-- .cd-products-columns -->
      </div> <!-- .cd-products-wrapper -->
      
      <ul class="cd-table-navigation">
        <li><a href="#0" class="prev inactive">Prev</a></li>
        <li><a href="#0" class="next">Next</a></li>
      </ul>
    </div> <!-- .cd-products-table -->
  </section> <!-- .cd-products-comparison-table -->
<script src="js/jquery-2.1.4.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>