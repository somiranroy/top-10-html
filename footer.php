<footer>
    <div class="container">
        <div class="footer-logo">
          <a href="/" class="footer__logo__link">
            Top10
          </a>
        </div>
        <div class="footer-detail">
          <div class="row">
            <div class="col-sm-4">
                <ul class="mobile-display-none">
                  <li>
                    <a href="">Blog</a>
                  </li>
                  <li>
                    <a href="">Deals</a>
                  </li>
                </ul>
                <div class="copyright-text">
                    Copyright © 2009-2020 Natural Intelligence Ltd. <br>All Rights Reserved.
                </div>
                <div class="social-media">
                  <ul>
                    <li>
                      <i class="fa fa-facebook-f"></i>
                    </li>
                    <li>
                      <i class="fa fa-twitter"></i>
                    </li>
                  </ul>
                </div>
            </div>
            <div class="col-sm-8">
                <ul class="mobile-display-none">
                  <li>
                    <a href="">About Us</a>
                  </li>
                  <li>
                    <a href="">Cookie Policy</a>
                  </li>
                  <li>
                    <a href="">Terms of Use</a>
                  </li>
                  <li>
                    <a href="">Privacy Policy</a>
                  </li>
                  <li>
                    <a href="">Sitemap</a>
                  </li>
                  <li>
                    <a href="">Partner With Us</a>
                  </li>
                  <li>
                    <a href="">Contact</a>
                  </li>
                </ul>
                <div class="design-to-help">
                   <p>
                     Designed to help users make confident decisions online, this website contains information about a wide range of products and services. Certain details, including but not limited to prices and special offers, are provided to us directly from our partners and are dynamic and subject to change at any time without prior notice. Though based on meticulous research, the information we share does not constitute legal or professional advice or forecast, and should not be treated as such.
                   </p>
                   <p>
                     Reproduction in whole or in part is strictly prohibited.
                   </p>
                   <p>
                     As an Amazon Associate we earn from qualifying purchases.
                   </p>
                </div>
            </div>
          </div>
        </div>
        <div class="footer-menu-for-mobile">
          <ul class="mobile-display-none">
              <li>
                <a href="">Blog</a>
              </li>
              <li>
                <a href="">Deals</a>
              </li>
              <li>
                <a href="">About Us</a>
              </li>
              <li>
                <a href="">Cookie Policy</a>
              </li>
              <li>
                <a href="">Terms of Use</a>
              </li>
              <li>
                <a href="">Privacy Policy</a>
              </li>
              <li>
                <a href="">Sitemap</a>
              </li>
              <li>
                <a href="">Partner With Us</a>
              </li>
              <li>
                <a href="">Contact</a>
              </li>
          </ul>
        </div>

    </div>
    
  </footer>
    
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> 
    
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.meal-slider').slick({
        arrows: false,
        centerPadding: "0px",
        dots: true,
        slidesToShow: 1,
        // autoplay: true,
        infinite: true
      });
    });
  </script>

  </body>
</html>