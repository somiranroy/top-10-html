<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title> Home </title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/9a1556e49d.js"></script>
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/phone.css" rel="stylesheet">

    <link href="css/tablet.css" rel="stylesheet">
    
     
  </head>
  <body>

  <header>
    <nav class="container navbar navbar-expand-md navbar-dark">
      <a class="navbar-brand" href="index.php"><img class="header__logo" src="https://images.top10.com/q_auto/v1/production/sites/uploads/photo/logo.20191212140001.svg" alt="Top10.com" title=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          
        </ul>
        <form class="form-inline mt-2 md-0">
          <div class="search-hidden-for-mobile searchbar">
              <i class="fa fa-search"></i>
              <input class="form-control mr-sm-2" type="text" placeholder="Search the best services" aria-label="Search">
          </div>
          
          <!-- <button class="btn " type="submit" ></button> -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SignupModal">
            Sign Up
          </button>

          <div class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div>
        </form>
      </div>
    </nav>
  </header>

  <div class="modal fade signup-modal" id="SignupModal" tabindex="-1" role="dialog" aria-labelledby="SignupModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Save time & money with the latest Top10.com lists</h5>
            <p>Sign up now and get the good stuff</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="signup-form">
              <input type="text" name="username" placeholder="Username" required>
              <input type="password" name="password" placeholder="Password" required>
              <input type="submit" value="Let's do this">
            </form>

            <div class="newsletters-count">
              <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
              <label for="vehicle1">Yes, I like newsletters, count me in.</label><br>
            </div> 
            <div class="click-for-terms-privacy">
              <p>
                By clicking sign up up you agree to our <a href="#">Terms of Use</a>& <a href="#">Privacy Policy</a>
              </p>
            </div>
          </div>
          <div class="modal-footer">
            <h6>
              Already a member?
              <!-- <button type="button" >
                Sign Up
              </button> -->
              <a href="#" class="btn" data-toggle="modal" data-target="#loginModal">Login</a>
            </h6>
          </div>
        </div>
      </div>
  </div>


  <div class="modal fade signup-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Go ahead, and do your log in thing</h5>
          
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="signup-form">
              <input type="text" name="username" placeholder="Email Address" required>
              <input type="password" name="password" placeholder="Password" required>
              <input type="submit" value="Log In">
            </form>
          </div>
          
        </div>
      </div>
  </div>