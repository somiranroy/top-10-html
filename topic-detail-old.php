<?php include 'header.php';?>
  <main class="main">
    <div class="container">
      <div class="breadcrumbs">
        <div class="breadcrumbs-data">
            <ul>
              <li>
                <a href="#" class="name">Home</a>
              </li>
              <li>
                <a href="#" class="name">Household</a>
              </li>
              <li>
                <span class="name">Meal Delivery</span>
              </li>
            </ul>
        </div>
        <div class="advertise-disclouser">
            <h6>
              We receive referral fees from partners
            </h6>
            <a href="#">
              Advertising Disclosure
            </a>
        </div>
      </div>
    </div>
    <div>
      <div class="article-page-banner container">
        <div>
        <img src="images/header-meal.jpg" class="article-">
        </div>
        <div class="article-title">
          <h1>
            Choose the Best Meal Delivery Service That Suits Your Lifestyle
          </h1>
          <div class="article-author">
              <div class="by-author">
                <div class="by-author__author-credentials">
                  <img class="by-author__image" src="images/Kerstin-Kuhn.webp">
                  <div class="author-data">
                    <div>
                      <a href="#"> Kerstin Kuhn</a>
                    </div>
                    <span>
                      Last updated: Jan. 01, 2020
                    </span>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <section class="topic-detail-banner container">
      <div class="container p-4 p-md-5 text-white rounded">
        <div class="col-md-6 px-0">
          <h1 class="">Choose from the best.
          </h1>
          <p class="lead my-3">Top10 lists for products and services to help you get
          through these challenging times</p>
        </div>
      </div>
    </section> -->
    <section class="best-meal-list">
      <div class="container pl-0">
        <div class="row">
          <div class="col-sm-8">
            <div class="increase-demand">
              <strong>
                Due to increased demand, some meal kit delivery services are currently unavailable. We will constantly update our rankings with new companies that can provide for your meal kit needs.
              </strong>
              <p>
                I  cooked and ate my way through the most popular meal delivery services so you can make an informed choice without having to try them all.
              </p>
              <h4>These are the best meal kit delivery services I tested and ranked:</h4>
              <p>1. Best for healthy eaters: <strong>Home Chef</strong></p>
              <p>2. Best for easy recipes: <strong>HelloFresh</strong></p>
              <p>3. Best for family and vegetarians: <strong>Sun Basket</strong></p>
              <p>4. Best for piping hot comfort foods: <strong>Fresh and Easy</strong></p>
              <p>5. Best for healthy eaters: <strong>Green Chef</strong></p>
              <p>6. Best for gluten-free meals: <strong>Freshly</strong></p>

            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="closer-look container pl-0">
      <div class="row">
        <div class="col-sm-8">
          <h1>
            A Closer Look at the Top 10 Meal Delivery Services
          </h1>
          <div class="all-meal-reviews">
            <ul>
              <li>
                <div class="meal-title">
                  <div class="flex">
                    <span>
                    1
                    </span>
                    <div class="name">
                      <div class="flex"> 
                        <h2>
                          Home Chef
                        </h2>
                        <div class="vistior-review">
                          <a href="#"><span>40 reviews</span></a>
                        </div>
                      </div>
                      <div class="healthy-eats">
                        <h6>Best for healthy eaters</h6>
                      </div>
                    </div>
                  </div>
                  
                </div>
                <div class="chef-logo">
                    <a href=""><img class="lazy mini-reviews__logo-image loaded" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" data-src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Home Chef" title="" data-was-processed="true"></a>
                </div>
              </li>
            </ul>
          </div>
          <div class="meal-images">
            <div class="meal-slider">
              <div>
                <img src="images/food1.jpg">
              </div>
              <div>
                <img src="images/food2.jpg">
              </div>
              <div>
                <img src="images/food3.jpg">
              </div>
            </div>
          </div>
          <div class="meal-content">
            
          </div>
        </div>
        <div class="col-sm-4">
          
        </div>
      </div>
      
    </section>


  </main>

  <?php include 'footer.php';?>