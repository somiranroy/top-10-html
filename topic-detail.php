<title>
  Detail
</title>
<?php include 'header.php';?>
  <main class="main">
    <section class="topic-banner">
      <div class="container p-4 p-md-5 text-white rounded">
        <div class="col-md-6 px-0">
          <h1 class="">Top 10 Best Airports in the World
          </h1>
          <p class="lead my-3">Whether you're traveling for business or for pleasure, connecting through one of the world's best airports can help to improve your experience.</p>
        </div>
    </div>
  </section>

  <section class="container all-article-list">
    <div class="row">
        <div class="col-sm-9">
            <div class="row article-data">
              <div class="col-sm-3">
                <div class="number">
                  <h6>1</h6>
                </div>
                  <img class="logo__image" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Singapore Changi" title="">

                  <div class="add-to-compare">
                    <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                    <label for="vehicle1"> Add to Compare</label><br>
                  </div>
              </div>
              <div class="col-sm-6 ">
                <h6><a>
                  Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them...
                </a></h6>

                <div class="best-of-article">
                 <ul>
                   <li>
                     Opened in: 1981
                   </li>
                   <li>
                     Best for: Fun attractions
                   </li>
                 </ul>
                </div>
              </div>
              <div class="col-sm-3 article-visit-site">
                <a href="">Visit site</a>
                <div class="expand-button">
                  <!-- <a href="#" class="collapsible" > Expand </a> -->
                  <button type="button" onclick="myFunction()">Expand</button>
                </div>
              </div>
            </div>
        </div>

        <section class="closer-look container pl-0" id="article-detail" style="display: none;">
            <div class="row">
              <div class="col-sm-8">
               <!--  <h1>
                  A Closer Look at the Top 10 Meal Delivery Services
                </h1> -->
                <div>
                      <div class="all-meal-reviews">
                        <ul>
                          <li>
                            <div class="meal-title">
                              <div class="flex">
                                <span>
                                1
                                </span>
                                <div class="name">
                                  <div class="flex"> 
                                    <h2>
                                      Home Chef
                                    </h2>
                                    <div class="vistior-review">
                                      <a href="#"><span>40 reviews</span></a>
                                    </div>
                                  </div>
                                  <div class="healthy-eats">
                                    <h6>Best for healthy eaters</h6>
                                  </div>
                                </div>
                              </div>
                              
                            </div>
                            <div class="chef-logo">
                                <a href=""><img class="lazy mini-reviews__logo-image loaded" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" data-src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Home Chef" title="" data-was-processed="true"></a>    
                            </div> 
                          </li>
                        </ul>
                      </div>
                      <div class="meal-images">
                        <div class="meal-slider">
                          <div>
                            <img src="images/food3.jpg">
                          </div>
                          <div>
                            <img src="images/food2.jpg">
                          </div>
                          <div>
                            <img src="images/food1.jpg">
                          </div>
                        </div>
                      </div>
                      <div class="review-plan-section">
                        <div class="pull-left">
                          <a href="#">
                            Read Home Chef Review
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="pull-right">
                          <a href="#" class="view-plan-page">view plans</a>
                        </div>
                      </div>
                      <div class="meal-content">
                        <div class="what-in-meal">
                          <ul>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                          </ul>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies. Plus there are options for all types of dietary specifications, including nut-free, gluten-free and dairy-free. The recipes are simple and easy to prepare, and some of the ingredients even arrive pre-cooked, which is a huge time saver. 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies.
                          </p>  
                        </div>
                        <div class="row pros-cons-list">
                          <div class="col-sm-6">
                            <div class="pros">
                              <h6>
                                Pros
                              </h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="cons">
                              <h6>Cons</h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              <div class="col-sm-4">
                
              </div>
            </div>
            
        </section>
        <!-- <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div> -->
        <div class="col-sm-3">
          
        </div>
    </div>


    <div class="row">
      <div class="col-sm-9">
          <div class="row article-data">
            <div class="col-sm-3">
              <div class="number">
                <h6>1</h6>
              </div>
                <img class="logo__image" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Singapore Changi" title="">

                <div class="add-to-compare">
                  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                  <label for="vehicle1"> Add to Compare</label><br>
                </div>
            </div>
            <div class="col-sm-6 ">
              <h6><a>
                Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them...
              </a></h6>

              <div class="best-of-article">
               <ul>
                 <li>
                   Opened in: 1981
                 </li>
                 <li>
                   Best for: Fun attractions
                 </li>
               </ul>
              </div>
            </div>
            <div class="col-sm-3 article-visit-site">
              <a href="">Visit site</a>
              <div class="expand-button">
                <!-- <a href="#" class="collapsible" > Expand </a> -->
                <button type="button" onclick="myFunction1()">Expand</button>
              </div>
            </div>
          </div>
      </div>

      <section class="closer-look container pl-0" id="article-detail1" style="display: none;">
            <div class="row">
              <div class="col-sm-8">
               <!--  <h1>
                  A Closer Look at the Top 10 Meal Delivery Services
                </h1> -->
                <div>
                      <div class="all-meal-reviews">
                        <ul>
                          <li>
                            <div class="meal-title">
                              <div class="flex">
                                <span>
                                1
                                </span>
                                <div class="name">
                                  <div class="flex"> 
                                    <h2>
                                      Home Chef
                                    </h2>
                                    <div class="vistior-review">
                                      <a href="#"><span>40 reviews</span></a>
                                    </div>
                                  </div>
                                  <div class="healthy-eats">
                                    <h6>Best for healthy eaters</h6>
                                  </div>
                                </div>
                              </div>
                              
                            </div>
                            <div class="chef-logo">
                                <a href=""><img class="lazy mini-reviews__logo-image loaded" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" data-src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Home Chef" title="" data-was-processed="true"></a>    
                            </div> 
                          </li>
                        </ul>
                      </div>
                      <div class="meal-images">
                        <div class="meal-slider">
                          <div>
                            <img src="images/food3.jpg">
                          </div>
                          <div>
                            <img src="images/food2.jpg">
                          </div>
                          <div>
                            <img src="images/food1.jpg">
                          </div>
                        </div>
                      </div>
                      <div class="review-plan-section">
                        <div class="pull-left">
                          <a href="#">
                            Read Home Chef Review
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="pull-right">
                          <a href="#" class="view-plan-page">view plans</a>
                        </div>
                      </div>
                      <div class="meal-content">
                        <div class="what-in-meal">
                          <ul>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                          </ul>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies. Plus there are options for all types of dietary specifications, including nut-free, gluten-free and dairy-free. The recipes are simple and easy to prepare, and some of the ingredients even arrive pre-cooked, which is a huge time saver. 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies.
                          </p>  
                        </div>
                        <div class="row pros-cons-list">
                          <div class="col-sm-6">
                            <div class="pros">
                              <h6>
                                Pros
                              </h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="cons">
                              <h6>Cons</h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              <div class="col-sm-4">
                
              </div>
            </div>
            
        </section>
      <!-- <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div> -->
      <div class="col-sm-3">
        
      </div>
    </div>


    <div class="row">
        <div class="col-sm-9">
            <div class="row article-data">
              <div class="col-sm-3">
                <div class="number">
                  <h6>1</h6>
                </div>
                  <img class="logo__image" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Singapore Changi" title="">

                  <div class="add-to-compare">
                    <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                    <label for="vehicle1"> Add to Compare</label><br>
                  </div>
              </div>
              <div class="col-sm-6 ">
                <h6><a>
                  Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them...
                </a></h6>

                <div class="best-of-article">
                 <ul>
                   <li>
                     Opened in: 1981
                   </li>
                   <li>
                     Best for: Fun attractions
                   </li>
                 </ul>
                </div>
              </div>
              <div class="col-sm-3 article-visit-site">
                <a href="">Visit site</a>
                <div class="expand-button">
                  <!-- <a href="#" class="collapsible" > Expand </a> -->
                  <button type="button" onclick="myFunction2()">Expand</button>
                </div>
              </div>
            </div>
        </div>

        <section class="closer-look container pl-0" id="article-detail2" style="display: none;">
            <div class="row">
              <div class="col-sm-8">
               <!--  <h1>
                  A Closer Look at the Top 10 Meal Delivery Services
                </h1> -->
                <div>
                      <div class="all-meal-reviews">
                        <ul>
                          <li>
                            <div class="meal-title">
                              <div class="flex">
                                <span>
                                1
                                </span>
                                <div class="name">
                                  <div class="flex"> 
                                    <h2>
                                      Home Chef
                                    </h2>
                                    <div class="vistior-review">
                                      <a href="#"><span>40 reviews</span></a>
                                    </div>
                                  </div>
                                  <div class="healthy-eats">
                                    <h6>Best for healthy eaters</h6>
                                  </div>
                                </div>
                              </div>
                              
                            </div>
                            <div class="chef-logo">
                                <a href=""><img class="lazy mini-reviews__logo-image loaded" src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" data-src="https://images.top10.com/f_auto,q_auto/v1/production/products/uploads/photo/home-chef.20180608095041.png" alt="Home Chef" title="" data-was-processed="true"></a>    
                            </div> 
                          </li>
                        </ul>
                      </div>
                      <div class="meal-images">
                        <div class="meal-slider">
                          <div>
                            <img src="images/food3.jpg">
                          </div>
                          <div>
                            <img src="images/food2.jpg">
                          </div>
                          <div>
                            <img src="images/food1.jpg">
                          </div>
                        </div>
                      </div>
                      <div class="review-plan-section">
                        <div class="pull-left">
                          <a href="#">
                            Read Home Chef Review
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="pull-right">
                          <a href="#" class="view-plan-page">view plans</a>
                        </div>
                      </div>
                      <div class="meal-content">
                        <div class="what-in-meal">
                          <ul>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                            <li>
                              <i class="fa fa-check" aria-hidden="true"></i> 
                              <strong>What’s in the box</strong>
                              2-6 meals2,4 or 6 servings each
                            </li>
                          </ul>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies. Plus there are options for all types of dietary specifications, including nut-free, gluten-free and dairy-free. The recipes are simple and easy to prepare, and some of the ingredients even arrive pre-cooked, which is a huge time saver. 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes 
                          </p>
                          <p>
                            Home Chef lets its customers set up a "taste profile” to help it to determine the meals best suited to them. Each week there is a selection of 18 different dishes to choose from, including quick, 5-minute lunches and entrées, and there are also add-ons such as smoothies.
                          </p>  
                        </div>
                        <div class="row pros-cons-list">
                          <div class="col-sm-6">
                            <div class="pros">
                              <h6>
                                Pros
                              </h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="cons">
                              <h6>Cons</h6>
                              <ul>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                                <li>
                                  <i class="fa fa-check" aria-hidden="true"></i> 
                                  Helps you build a “taste profile” to match you with recipes
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              <div class="col-sm-4">
                
              </div>
            </div>
            
        </section>
        <!-- <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div> -->
        <div class="col-sm-3">
          
        </div>
    </div>


    

    <section class="item-comparison-table">
      <?php include 'comparison-table.php';?>
    </section>

  </section>

 <!-- <script>
   var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}
 </script> -->

 <script>
function myFunction() {
  var x = document.getElementById("article-detail");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function myFunction1() {
  var x = document.getElementById("article-detail1");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function myFunction2() {
  var x = document.getElementById("article-detail2");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<?php include 'all-category-box.php';?>
  <?php include 'footer.php';?>